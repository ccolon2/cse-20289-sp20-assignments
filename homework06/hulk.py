#!/usr/bin/env python3

import concurrent.futures
import hashlib
import os
import string
import sys

# Constants

ALPHABET = string.ascii_lowercase + string.digits
# Functions

def usage(exit_code=0):
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [-a ALPHABET -c CORES -l LENGTH -p PATH -s HASHES]
    -a ALPHABET Alphabet to use in permutations
    -c CORES    CPU Cores to use
    -l LENGTH   Length of permutations
    -p PREFIX   Prefix for all permutations
    -s HASHES   Path of hashes file''')
    sys.exit(exit_code)

def md5sum(s):
    ''' Compute md5 digest for given string. '''
    # TODO: Use the hashlib library to produce the md5 hex digest of the given
    # string.
    h=str(s).encode()
    h=hashlib.md5(h)
    return h.hexdigest()

def permutations(length, alphabet=ALPHABET):
    ''' Recursively yield all permutations of the given length using the
    provided alphabet. '''
    # TODO: Use yield to create a generator function that recursively produces
    # all the permutations of the given length using the provided alphabet.
    if length > 1:
        for firstletter in alphabet:
            for rest in permutations(length-1,alphabet):
                yield(firstletter+rest)
    else:
        for letter in alphabet:
            yield letter



def flatten(sequence):
    ''' Flatten sequence of iterators. '''
    # TODO: Iterate through sequence and yield from each iterator in sequence.
    for obj in sequence:
        yield from obj

def crack(hashes, length, alphabet=ALPHABET, prefix=''):
    ''' Return all password permutations of specified length that are in hashes
    by sequentially trying all permutations. '''
    # TODO: Return list comprehension that iterates over a sequence of
    # candidate permutations and checks if the md5sum of each candidate is in
    # hashes.
    #create list comprehension
    combos= permutations(length,alphabet)
       
    

    # [   for item in combos if in hashes]
    return [prefix+x for x in combos if md5sum(prefix+x) in hashes]

def cracker(arguments):
    ''' Call the crack function with the specified arguments '''
    return crack(*arguments)

def smash(hashes, length, alphabet=ALPHABET, prefix='', cores=1):
    ''' Return all password permutations of specified length that are in hashes
    by concurrently subsets of permutations concurrently.
    '''
    # TODO: Create generator expression with arguments to pass to cracker and
    # then use ProcessPoolExecutor to apply cracker to all items in expression.
    arguments =  ((hashes,length-1,alphabet,prefix+n) for n in alphabet)
        
    with concurrent.futures.ProcessPoolExecutor(cores) as executor:
        return flatten(executor.map(cracker, arguments))
  
def main():
    # TODO: Parse command line arguments
    arguments   = sys.argv[1:]
    alphabet    = ALPHABET
    cores       = 1
    hashes_path = 'hashes.txt'
    length      = 1
    prefix      = ''
    args = arguments
    while len(args) and args[0].startswith('-'):
        arg=args.pop(0)    
        if arg == '-a':
            alphabet=str(args.pop(0))
        elif arg == '-c':
            cores=int(args.pop(0))
        elif arg == '-l':
            length=int(args.pop(0))
        elif arg == '-s':
            hashes=str(args.pop(0))
        elif arg == '-p':
            prefix=str(args.pop(0))
        elif arg == '-h':
            usage(0)
        else:
            usage(1)


    
    # TODO: Load hashes set
    hashes_set = set(line.strip() for line in open(hashes_path))
    # TODO: Execute crack or smash function
        
    if cores > 1 and length > 1:
        passwords = smash(hashes_set, length, alphabet, prefix, cores)
    else: 
        passwords = crack(hashes_set, length, alphabet, prefix)

        
    # TODO: Print all found passwords
    for password in passwords:
        print(password)

    # Main Execution
if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
