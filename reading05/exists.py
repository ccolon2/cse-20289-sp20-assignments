#!/usr/bin/env python3

import sys


if(len(sys.argv) == 1):

	exit(1)
else:
	for arg in sys.argv:
		try:
			file = open(arg, 'r')
			print("{} exists!".format(arg))
		except:
			print("{} does not exist!".format(arg))
			exit(1)

