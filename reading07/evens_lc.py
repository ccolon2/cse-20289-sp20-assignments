#!/usr/bin/env python3
import sys

print(' '.join(
    # TODO: One-line expression with list comprehension
                [r.strip() for r in sys.stdin if int(r) % 2 == 0] 
)) 

