#!/usr/bin/env python3
import sys

def evens(stream):
    # TODO: Implementation that uses yield statement
        for num in stream:
                if int(num) % 2 == 0:
                        yield num.strip()

print(' '.join(evens(sys.stdin))) 

