#!/usr/bin/env python3

import sys

print(' '.join(
    # TODO: One-line expression with map, filter, lambda
                filter(lambda e: int(e) % 2 == 0, map(lambda r: r.strip(), sys.stdin))
))

