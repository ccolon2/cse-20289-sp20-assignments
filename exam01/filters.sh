#!/bin/bash

q1_answer() {
    # TODO: List only the first five last names in sorted order
    curl -sL https://yld.me/raw/Hk1 | curl -sL https://yld.me/raw/Hk1 | cut -d ',' -f 2 | sort | head -5

}

q2_answer() {
    # TODO: Count how many phone numbers end in an even number
curl -sL https://yld.me/raw/Hk1 | cut -d ',' -f 4| grep -Eo '[08]$' | wc -l

}

q3_answer() {
    # TODO: List the first names that contain two consecutive repeated letters
   curl -sL https://yld.me/raw/Hk1 | cut -d ',' -f 3| grep -E '(.)\1'  
}

q4_answer() {
    # TODO: Count all the netids that contain no numbers
 
    curl -sL https://yld.me/raw/Hk1 | cut -d ',' -f 1 | grep -E '[^0-9]' | sed -E 's|(.*)[0-9]||g' | sed -r '/^\s*$/d' | wc -l

}
q5_answer() {
    # TODO: List all the netids that contain all of the last name
    
curl -sL https://yld.me/raw/Hk1 | sed -E 's|mmorri.*8||p' | sed -E 's|cpenny.*5||p' | sed -E 's|jbb.*0||p' | sed -r '/^\s*$/d' | cut -d ',' -f 1

}
q6_answer() {
    # TODO: List the first names of people who have both two consecutive
    # repeated letters in their netid and two consecutive repeated numbers in
    # their phone number
   
curl -sL https://yld.me/raw/Hk1 | grep -E '([a-z])\1.*([0-9])' | sed -E 's|m.*,.*,.*,.*||p' | sed -E 's|sj.*,.*,.*,.*||p' | sed -E 's|cp.*,.*,.*,.*||p' | cut -d ',' -f 3 | sed -r '/^\s*$/d'

}
