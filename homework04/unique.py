#!/usr/bin/env python3

import os
import sys

# Functions

def usage(status=0):
    ''' Display usage message and exit with status. '''
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [flags]

    -c      Prefix lines by the number of occurences
    -d      Only print duplicate lines
    -i      Ignore differences in case when comparing, prints out full line in lowercase
    -u      Only print unique lines

By default, {progname} prints one of each type of line.''')
    sys.exit(status)

def count_frequencies(stream=sys.stdin, ignore_case=False):
    ''' Count the line frequencies from the data in the specified stream while
    ignoring case if specified. '''
    freq = {}
    
    for x in stream:
        x = x.rstrip()
        if ignore_case: 
            x = x.lower()
        if(x in freq):
            freq[x] += 1
        else:
            freq[x] = 1

   
    return freq


def print_lines(frequencies, occurrences=False, duplicates=False, unique_only=False):
    ''' Display line information based on specified parameters:

    - occurrences:  if True, then prefix lines with number of occurrences
    - duplicates:   if True, then only print duplicate lines
    - unique_only:  if True, then only print unique lines
    '''
    for key,value in frequencies.items():
        if duplicates and value==1: #if d flag is specified
            continue
        if unique_only and value >1: #if u flag is specified
            continue
        if occurrences: #if c flag is specified
            print(f'{value:>7} {key}') 
        else:
            print(key)
                   
        
    

def main():
    ''' Process command line arguments, count frequencies from standard input,
    and then print lines. '''
    #Sets default values
    occurrences=False 
    duplicates=False
    frequencies=False
    ignore_case=False
    unique_only=False
    arguments = sys.argv[1:]

    while len(arguments) and arguments[0].startswith('-'):
        argument = arguments.pop(0)

        if argument == '-c':
            occurrences=True
        elif argument == '-d':
            duplicates=True
        elif argument == '-u':
            unique_only=True
        elif argument == '-i':
            ignore_case=True
        elif argument == '-h':
            usage(0)
            
    freqdictionary=count_frequencies(sys.stdin, ignore_case)
    print_lines(freqdictionary,occurrences,duplicates,unique_only)

# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
