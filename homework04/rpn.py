#!/usr/bin/env python3
import os
import sys

# Globals

OPERATORS = {'+', '-', '*', '/'}

# Functions

def usage(status=0):
    ''' Display usage message and exit with status. '''
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname}

By default, {progname} will process expressions from standard input.''')

    sys.exit(status)

def error(message):
    ''' Display error message and exit with error. '''
    print(message, file=sys.stderr)
    sys.exit(1)

def evaluate_operation(operation, operand1, operand2):
    ''' Return the result of evaluating the operation with operand1 and
    operand2.
    
    >>> evaluate_operation('+', 4, 2)
    6

    >>> evaluate_operation('-', 4, 2)
    2

    >>> evaluate_operation('*', 4, 2)
    8

    >>> evaluate_operation('/', 4, 2)
    2.0
    '''
    if operation=='+':
        return operand1+operand2
    elif operation=='-':
        return operand1-operand2
    elif operation=='*':
        return operand1*operand2
    elif operation=='/':
        return operand1/operand2

def evaluate_expression(expression):
    ''' Return the result of evaluating the RPN expression.
    
    
    >>> evaluate_expression('4 2 +')
    6.0

    >>> evaluate_expression('4 2 -')
    2.0

    >>> evaluate_expression('4 2 *')
    8.0

    >>> evaluate_expression('4 2 /')
    2.0

    >>> evaluate_expression('4 +')
    Traceback (most recent call last):
    ...
    SystemExit: 1

    >>> evaluate_expression('a b +')
    Traceback (most recent call last):
    ...
    SystemExit: 1
    '''
    s=[]
    operator = 0
    split = expression.split()
    	
    for x in expression.split(): 
        if x in OPERATORS:
            if len(s)>=2: #verifies at least 2 operands are present to do an operation
                operand1=s.pop()
                operand2=s.pop()
                result=evaluate_operation(x,operand2,operand1)
                s.append(result)
            else:
                error("Invalid amount of operands")
        else:
            try: 
                number=float(x) 
                s.append(number)
            except ValueError:
                error("Unable to convert the value")

    return s[-1]

    
def main():#check flag
    ''' Parse command line arguments and process expressions from standard
    input. '''
    arguments=sys.argv[1:]
    
    while len(arguments) and arguments[0].startswith('-'):
        argument = arguments.pop(0)
        if argument == '-h':
            usage(0)
    for line in sys.stdin:
        answer=evaluate_expression(line)# Main Execution
        print(answer)
	

if __name__ == '__main__':
    main()

