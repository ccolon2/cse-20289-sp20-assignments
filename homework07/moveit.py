#!/usr/bin/env python3
import atexit
import os
import sys
import tempfile

# Functions

def usage(status=0):
    ''' Display usage message for program and exit with specified status. '''
    print(f'Usage: {os.path.basename(sys.argv[0])} files...')
    sys.exit(status)

def save_files(files):
    ''' Save list of files to a temporary file and return the name of the
    temporary file. '''
    with tempfile.NamedTemporaryFile(delete=False) as tf:
        for f in files:
            tf.write(f.encode())
            tf.write('\n'.encode())
    return tf.name  
        
def edit_files(path):
    ''' Fork a child process to edit the file specified by path using the user's
    default EDITOR (use "vim" if not set).  Parent waits for child process and
    returns whether or not the child was successful. '''
    try:
        pid = os.fork()
        
    except OSError:
        sys.exit(1)
    if pid == 0: #Child Process
        os.execlp(os.environ.get('EDITOR',"vim"),'test',path)

    else: #Parent
        pid, status = os.wait()
        return os.WEXITSTATUS(status)


def move_files(files, path):
    ''' For each file in files and the corresponding information from the file
    specified by path, rename the original source file to the new target path
    (if the names are different).  Return whether or not all files were
    successfully renamed. '''
    tempfiles=[]
    with open(path) as f:
        for line in f:
            tempfiles.append(line.rstrip()) 

    result = zip(files,tempfiles)
    for f in result:
        print(f)
        if f[0] != f[1]:
            try:
                os.rename(f[0],f[1])
            except OSError:
                sys.exit(1)
    return True

def main():
    ''' Parse command line arguments, save arguments to temporary file, allow
    the user to edit the temporary file, move the files, and remove the
    temporary file. '''
    # TODO: Parse command line arguments

    arguments = sys.argv[1:]
    if not arguments:
        usage(0)

    files = []
    while len(arguments):
        argument = arguments.pop(0)
        if argument == '-h':
            usage(0)
        else:
            files.append(argument)
   



                
        

    # TODO: Save files (arguments)
    tempfilename = save_files(files)

    # TODO: Register unlink to cleanup temporary file
    atexit.register(os.unlink,tempfilename)

    # TODO: Edit files stored in temporary file
    edit_files(tempfilename)

    # TODO: Move files stored in temporary file
    move_files(files,tempfilename)

# Main Execution

if __name__ == '__main__':
    main()
