#!/usr/bin/env python3
import os
import sys

# TODO: Determine which directory to walk from command line argument
args = '.'

if len(sys.argv) > 1:
    args = sys.argv[1:]
contents = os.scandir(args[0])
ls = []
try:
    with contents as entries:
        for entry in sorted(entries,key = lambda entry : entry.name):
            ls.append(entry.name)
        for i in ls:
            print(i)
except OSError:
    sys.exit(1)

# TODO: Walk specified directory in sorted order and print out each entry's
# file name

