/* library.c: string utilities library */

#include "str.h"
#include <ctype.h>
#include <string.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 **/
void	str_lower(char *s) {
    while(*s){
       *s=tolower(*s);
        s++;
    }
}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 **/
void	str_upper(char *s) {
     while(*s){
       *s=toupper(*s);
        s++;
    }
}

/**
 * Convert all characters in string to titlecase.
 * @param   s       String to convert
 **/
void	str_title(char *s) {
    char *a = s;
   
    while(*a){
        
        while(*a && !isalpha(*a)){
            a++;

        }
        if(*a){
            *a = toupper(*a);
            a++;
        }

        while(*a && isalpha(*a)){
            *a = tolower(*a);
             a++;
        }
   }

}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 **/
void    str_chomp(char *s) {
    char *end = s + strlen(s)-1;
    if(*end=='\n' && end >= s){
        *end = 0;
    }
}


/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 **/
void    str_strip(char *s) {

   char *reader = s;
   char *writer = s;


   
   while (*reader!='\0' && isspace(*reader)){
        reader++;
        
   }

    while (*reader!='\0'){
        *writer = *reader;
        writer++;
        reader++;
    }
    reader--;
    writer--;
    while(isspace(*writer)){

        writer--;
    }
    writer++;
    *writer='\0';
}



/**
 * Reverses a string.
 * @param   s       String to reverse
 **/
void    str_reverse(char *s) {
    char *head = s;
    char *tail = s + strlen(s) - 1;
    char dummy;

    while (head < tail) {
        dummy = *tail;
        *tail = *head;
        *head = dummy;

        head++;
        tail--;
    }

}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 **/
void    str_translate(char *s, char *from, char *to) {
    int lookup [1<<8] = {0};

    for (char *c = from; *c && *to; c++) {
        lookup [(int)*c] = *to;
        to++;
    }

    for (char *c = s; *c; c++) {
        if (lookup[(int)*c] != 0){
            *c = lookup[(int)*c];
        }
    }
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	str_to_int(const char *s, int base) {
    int sum = 0;
    int exponent = 1;
    int number = 0;

    for (const char *a = s + strlen(s)-1; a >= s; a--) {
        if (isalpha(*a)){
            number = toupper(*a) - 55;
        }

        else{
            number = *a - '0';
        }
    
        sum = sum + number * exponent;
        exponent = exponent *  base;
    }
    
    return sum;
    return 0;
}


/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
