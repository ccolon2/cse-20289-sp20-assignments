/* main.c: string library utility */

#include "str.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;

/* Flags */
enum {
    /* TODO: Enumerate Flags*/
    STRIP = 1<<1,
    REVERSE = 1<<2,
    LOWERCASE = 1<<3,
    UPPERCASE = 1<<4,
    TITLECASE = 1<<5
};

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s      Strip whitespace\n");
    fprintf(stderr, "   -r      Reverse line\n");
    fprintf(stderr, "   -l      Convert to lowercase\n");
    fprintf(stderr, "   -u      Convert to uppercase\n");
    fprintf(stderr, "   -t      Convert to titlecase\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int flag) {
    /* TODO: Process each line in stream by performing transformations */
    char buffer[BUFSIZ];
    while(fgets(buffer,BUFSIZ,stream)!=NULL) {
        str_translate(buffer,source,target);
        if(flag & STRIP){
            str_strip(buffer);
        }
        if(flag & REVERSE){
            str_chomp(buffer);
            str_reverse(buffer);
        }
       if(flag & LOWERCASE){
            str_lower(buffer);
        }
        if(flag & UPPERCASE){
            str_upper(buffer);
          
        }
        if(flag & TITLECASE){
            str_title(buffer);
        }
      str_chomp(buffer);
      puts(buffer);  
    }
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* TODO: Parse command line arguments */
    char*  source="";
    char* target="";
    

    int argind=1;
    int flag = 0;
    while(argind < argc && argv[argind][0] == '-') { 
        char letter = argv[argind][1]; 
       switch(letter){
           case 'h':
               usage(0);
               break;
           case 's':
               flag|=STRIP;
               break;
           case 'r':
               flag|=REVERSE;
               break;
           case 'l':
               flag|=LOWERCASE;
               break;
           case 'u':
               flag|=UPPERCASE;
               break;
           case 't':
               flag|=TITLECASE;
               break;
         
       }
   
       argind++;
    }

    if(argind<argc){
        source = argv[argind];
        target = argv[argind+1];
    }


    /* TODO: Translate Stream */
    translate_stream(stdin,source,target,flag);
       


    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
