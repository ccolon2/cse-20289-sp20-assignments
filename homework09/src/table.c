/* table.c: Separate Chaining Hash Table */

#include "table.h"
#include "hash.h"
#include "macros.h"

/**
 * Create Table data structure.
 * @param   capacity        Number of buckets in the hash table.
 * @return  Allocated Table structure.
 */
Table *	    table_create(size_t capacity) {
    if(capacity==0){
        capacity = DEFAULT_CAPACITY;
    }
    Table *table = calloc(1,sizeof(Table));

    if (table==NULL){

        return NULL;
    }
    table->capacity=capacity;

    table->buckets = calloc(capacity,sizeof(Pair));
    if(table->buckets==NULL){
        return NULL;
    }
    return table;
}

/**
 * Delete Table data structure.
 * @param   t               Table data structure.
 * @return  NULL.
 */
Table *	    table_delete(Table *t) {
    for(int i=0; i< t->capacity; i++)
         pair_delete(t->buckets[i].next, true);
    free(t->buckets);
    free(t);
    return t;
}

/**
 * Insert or update Pair into Table data structure.
 * @param   m               Table data structure.
 * @param   key             Pair's key.
 * @param   value           Pair's value.
 * @param   type            Pair's value's type.
 */
void        table_insert(Table *t, const char *key, const Value value, Type type) {
    int bucket_location;
    bucket_location = hash_from_data(key,strlen(key))%t->capacity;
    Pair * curr;
    curr = &t->buckets[bucket_location];
    while(curr->next!=NULL){  
        curr=curr->next;
        if(strcmp(curr->key, key)==0){
            pair_update(curr, value, type);
            return;
        }
    }
    t->size++;
    curr->next = pair_create(key, value, NULL, type);


}

/**
 * Search Table data structure by key.
 * @param   t               Table data structure.
 * @param   key             Key of the Pair to search for.
 * @return  Pointer to the Value associated with specified key (or NULL if not found).
 */
Value *     table_search(Table *t, const char *key) {
    int bucket_location;
    bucket_location = hash_from_data(key, strlen(key))%(t->capacity);
    Pair *curr;
    curr = t->buckets[bucket_location].next;
    while(curr!=NULL){
         if(strcmp(curr->key, key)==0) 
            return &curr->value;
        
        curr = curr->next;
    }

    return NULL;
}

/**
 * Remove Pair from Table data structure with specified key.
 * @param   t               Table data structure.
 * @param   key             Key of the Pair to remove.
 * l<F4>
 * return   Whether or not the removal was successful.
 */
bool        table_remove(Table *t, const char *key) {
    int bucket_location;
    bucket_location = hash_from_data(key, strlen(key))%t->capacity; 
    Pair* previous = &t->buckets[bucket_location];
    for(Pair *curr = t->buckets[bucket_location].next;curr;curr=curr->next){
        if(strcmp(curr->key, key)==0) {
            previous->next = curr->next;
            pair_delete(curr, false);
            t->size--;
            return true;
        }
        else{
            previous = curr;
        }
    }
    return false;
}

/**
 * Format all the entries in the Table data structure.
 * @param   m               Table data structure.
 * @param   stream          File stream to write to.
 */
void	    table_format(Table *t, FILE *stream) {
    for(int i =0; i < t->capacity; i++){
        for(Pair *curr=t->buckets[i].next;curr;curr = curr->next)
           pair_format(curr,stream);
        
    }

}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
