/* duplicates.c */

#include "hash.h"
#include "macros.h"
#include "table.h"

#include <dirent.h>
#include <limits.h>
#include <sys/stat.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Structures */

typedef struct {
    bool count;
    bool quiet;
} Options;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s paths...\n", PROGRAM_NAME);
    fprintf(stderr, "    -c     Only display total number of duplicates\n");
    fprintf(stderr, "    -q     Do not write anything (exit with 0 if duplicate found)\n");
    exit(status);
}

/**
 * Check if path is a directory.
 * @param       path        Path to check.
 * @return      true if Path is a directory, otherwise false.
 */
bool is_directory(const char *path) {
    struct stat stat_path;
    if(stat(path,&stat_path)<0){
        return 0;
    }
    return S_ISREG(stat_path.st_mode);
}

/**
 * Check if file is in table of checksums.
 *
 *  If quiet is true, then exit if file is in checksums table.
 *
 *  If count is false, then print duplicate association if file is in
 *  checksums table.
 *
 * @param       path        Path to file to check.
 * @param       checksums   Table of checksums.
 * @param       options     Options.
 * @return      0 if Path is not in checksums, otherwise 1.
 */
size_t check_file(const char *path, Table *checksums, Options *options) { 
    char hexdigest[HEX_DIGEST_LENGTH];
    if(!hash_from_file(path,hexdigest)){
        return 0;
    }
    Value *value = table_search(checksums,hexdigest);
    bool truthval = false;
    if(value!=NULL){
        truthval=true;
    }
    if(options->quiet==true && truthval==true){
        exit(0);
        
    }
        
        else{
            Value val;
            val.string = (char*)path;
            table_insert(checksums,hexdigest,val,STRING);
        
        }
    
    if(options->count==false){
        if(value!=NULL){
            
            printf("%s is a duplicate of %s\n",path,value->string);
        }
    }
    return 0;


}

/**
 * Check all entries in directory (recursively).
 * @param       root        Path to directory to check.
 * @param       checksums   Table of checksums.
 * @param       options     Options.
 * @return      Number of duplicate entries in directory.
 */
size_t check_directory(const char *root, Table *checksums, Options *options) {
    char buf[BUFSIZ];
    DIR *d = opendir(root);
    size_t duplicates = 0;
    if(!d){
        return 0;
    }
    for(struct dirent *e=readdir(d);e;e=readdir(d)){
            if(strcmp(e->d_name,".")== 0 || strcmp(e->d_name,"..")==0){
                continue;
            }
            sprintf(buf,"%s/%s",root,e->d_name);
            if(is_directory(buf)){
                duplicates += check_directory(buf,checksums,options);
            
            }
           else{
                duplicates += check_file(buf,checksums,options);


           }
            
    }
    closedir(d);
    return duplicates;
   

}

/* Main Execution */

int main(int argc, char *argv[]) 
{
    /* Parse command line arguments */
    int argind = 1;
    Options option;
    option.quiet = false;
    option.count = false;
    while(argind < argc && argv[argind][0] == '-'){
        char letter = argv[argind][1];
        switch(letter){
            case 'h':
                usage(0);
                break;
            case 'q':
                option.quiet = true;
                break;
            case 'c':
                option.count = true;
                break;
        }
        argind++;
    }
    /* Check each argument */
    Table *table = table_create(0);
    size_t duplicates = 0;
    while(argind < argc){
       if(is_directory(argv[argind])){
           duplicates += check_directory(argv[argind],table,&option);
        }
       else{
           duplicates += check_file(argv[argind],table,&option);
        }

       argind++;
    }
    /* Display count if necessary */
    table_delete(table);
    if(option.count){
        printf("%zu\n",duplicates);
    }

    return EXIT_SUCCESS;
    
}


/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
