#!/usr/bin/env python3 
import requests
import os
import re

# Constants

URL = 'http://catalog.cse.nd.edu:9097/query.json'

# TODO: Make a HTTP request to URL
response = requests.get(URL)

# TODO: Access json representation from response object
data = response.text

# TODO: Display all machine names with type "wq_factory"
regex=re.compile(r'{"name":"([^"]+)".*"type":"wq_factory".*')
result=regex.findall(data)
for line in result:
        print(line)
