#!/bin/sh

# Globals

URL=https://www.zipcodestogo.com/
STATE=Indiana
CITY=""

# Functions

usage() {
	cat 1>&2 <<EOF
  Usage: $(basename $0) 	
  -c      CITY    Which city to search
  -s      STATE   Which state to search (Indiana)

If no CITY is specified, then all the zip codes for the STATE are displayed.
EOF
	exit $1		
}
zipcode_information(){
	curl -s https://www.zipcodestogo.com/"$STATE"/
}
state(){
	zipcode_information | sed -En 's|.*/.*/([0-9]+)/">.*|\1|p'   

}
city(){
 	curl -s https://www.zipcodestogo.com/"$STATE"/ | grep "/$CITY/" | grep -Eo "[0-9]{5}" | sort | uniq
}


while [ $# -gt 0 ]; do
    case $1 in
    -h) usage 0;;
    -s) STATE=$(echo $2 | sed 's/ /%20/g'); shift;;
    -c)CITY=$2;
    shift;;
     *) usage 1;;
    
     
    esac
    shift
done

# Filter Pipeline(s)
if [ -z "$CITY" ]; then
	state
else 
	city
fi


#curl -s $URL"$STATE"/ | sed -En 's|.*/.*/([0-9]+)/">.*|\1|p' 
