#!/usr/bin/env python3

import os
import sys
import pprint
import requests

# Constants

ISGD_URL = 'http://is.gd/create.php'
headers={'user-agent': 'reddit-{}'.format(os.environ.get('USER', 'cse-20289-sp20'))}

# Functions
def usage(status=0):
    ''' Display usage information and exit with specified status '''
    print('''Usage: {} [options] URL_OR_SUBREDDIT

    -s          Shorten URLs using (default: False)
    -n LIMIT    Number of articles to display (default: 10)
    -o ORDERBY  Field to sort articles by (default: score)
    -t TITLELEN Truncate title to specified length (default: 60)
    '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

def load_reddit_data(url):
    ''' Load reddit data from specified URL into dictionary
    
    >>> len(load_reddit_data('https://reddit.com/r/nba/.json'))
    27

    >>> load_reddit_data('linux')[0]['data']['subreddit']
    'linux'
    '''
    # TODO: Verify url parameter (if it starts with http, then use it,
    # otherwise assume it is just a subreddit).
   # URL= f'https://reddit.com/r/{url}/.json'
    if url[0:4]=='http':
        URL=url
    else: 
        URL= f'https://reddit.com/r/{url}/.json'
        

    data=requests.get(URL,headers=headers)
    converted = data.json()
    #pprint.pprint(converted['data'])
    #pprint.pprint(converted['data']['children'])
    return converted['data']['children']

def shorten_url(url):
    ''' Shorten URL using is.gd service

    >>> shorten_url('https://reddit.com/r/aoe2')
    'https://is.gd/dL5bBZ'

    >>> shorten_url('https://cse.nd.edu')
    'https://is.gd/3gwUc8'
    

    '''
    short = requests.get(ISGD_URL,params={'format':'json','url':url})
    data = short.json()
    shortened= data['shorturl']

    # TODO: Make request to is.gd service to generate shortened url.
    return shortened

def print_reddit_data(data, limit=10, orderby='score', titlelen=60, shorten=False):
    ''' Dump reddit data based on specified attributes '''
    # TODO: Sort articles stored in data list by the orderby key, and then
    # print out each article's index, title, score, and url using the following
    # format:
    #
    #   print(f'{index:4}.\t{title} (Score: {score})\n\t{url}')
    #
    # Note: Trim or modify the output based on the keyword arguments to the function.
    if orderby=='score':
        reverse=True
    else:
        reverse=False
    # pprint.pprint(data)
    
    articles = sorted(data,key=lambda i: i['data'][orderby],reverse=reverse)
    for index, entry in enumerate(articles[:limit],1):
        title=entry['data']['title']
        restriction=title[0:titlelen]
        if shorten:
            print(f'{index:4}.\t{restriction} (Score: {entry["data"]["score"]})\n\t{shorten_url(entry["data"]["url"])}')

        else:
            print(f'{index:4}.\t{restriction} (Score: {entry["data"]["score"]})\n\t{entry["data"]["url"]}')
       
        if index != limit:
            print( )
  
def main():
    # TODO: Parse command line arguments
    arguments = sys.argv[1:]
    url       = None
    limit     = 10
    orderby   = 'score'
    titlelen  = 60
    shorten   = False

    # TODO: Load data from url and then print the data
    # print('hey')
   
    #shorten_url('https://reddit.com/r/nba/.json')
    arguments=sys.argv[1:]
    #if sys.argv[0] is sys.argv[-1]:
       # usage(0)
    while len(arguments) and arguments[0].startswith('-'):
        argument=arguments.pop(0)
        if argument == '-h':
            usage(0)
        elif argument=='-s':
            shorten= True
        elif argument == '-o':
            argument=arguments.pop(0)
            orderby=argument
        elif argument == '-t':
            argument=arguments.pop(0)
            titlelen=int(argument)
        elif argument == '-n':
            limit=int(arguments.pop(0))
        else:
            usage(1)
    if not arguments:
        usage(1)
    data=load_reddit_data(arguments[0])
    print_reddit_data(data, limit, orderby, titlelen, shorten)
# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
