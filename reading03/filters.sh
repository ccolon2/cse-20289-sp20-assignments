#!/bin/bash

q1_answer() {
    # TODO: Complete pipeline
    echo "All your base are belong to us" | tr a-z A-Z
}

q2_answer() {
    # TODO: Complete pipeline
    echo "monkeys love bananas" | sed 's/monkeys/gorillaz/'
 }

q3_answer() {
    # TODO: Complete pipeline
    echo "monkeys love bananas" | sed 's/^ *//'
}

q4_answer() {
    # TODO: Complete pipeline
    curl -sLk https://yld.me/raw/yWh | awk -F":" '$1=="root"{print $7}'
}

q5_answer() {
    # TODO: Complete pipeline
    curl -sLk https://yld.me/raw/yWh | sed 's/\(\/bin\/bash\|\/bin\/csh\|\/bin\/tcsh\)$/\/usr\/bin\/python/'| grep python 
}

q6_answer() {
    # TODO: Complete pipeline
    curl -sLk https://yld.me/raw/yWh | grep 4[0-9]*7
}
